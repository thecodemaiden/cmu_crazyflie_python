# -*- coding: utf-8 -*-
#
#     ||          ____  _ __
#  +------+      / __ )(_) /_______________ _____  ___
#  | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
#  +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
#   ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
#
#  Copyright (C) 2014 Bitcraze AB
#
#  Crazyflie Nano Quadcopter Client
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA  02110-1301, USA.

"""
Simple example that connects to the first Crazyflie found, ramps up/down
the motors and disconnects.
"""

import time, sys
from threading import Thread

sys.path.append('../crazyflie-clients-python/lib')
import cflib
from cflib.crazyflie import Crazyflie

import logging
from cfclient.utils.logconfigreader import LogConfig

logging.basicConfig(level=logging.ERROR)

import pygame as pg
from cube_utils import Point3D, OrientationCubeView, EulerAngle
from operator import itemgetter

from collections import namedtuple
import math

from pid import PIDController
from commander import CrazyflieController


def makeLabel(text, size=36):
    font = pg.font.Font(None, size)
    label = font.render(text, 1, (255,255,255))
    return label

def accToEuler(acc, yaw=0):
    p = math.atan2(acc[0], math.sqrt(acc[2]*acc[2]+acc[1]*acc[1]))
    r = math.atan2(-acc[1],math.sqrt(acc[2]*acc[2]+acc[0]*acc[0]))
    #p = math.atan2(acc[1], math.sqrt(acc[0]*acc[0]+acc[2]*acc[2]))
    #r = math.atan2(-acc[0], acc[2])

    p = p * (180.0/math.pi)
    r = r * (180.0/math.pi)
    return EulerAngle(yaw=yaw, pitch=p, roll=r)

def drawBattery(screen, val, startPos = (0,0)):
    l = makeLabel('Battery: {:1.2f}V'.format(val))
    x,y = startPos
    screen.blit(l, (x,y))
    y = y + l.get_rect().height

    return (x + l.get_rect().width, y)
    
def drawPressure(screen, val, startPos = (0,0)):
    l = makeLabel('Pressure: {:3.1f} mmHg'.format(val))
    x,y = startPos
    screen.blit(l, (x,y))
    y = y + l.get_rect().height

    return (x + l.get_rect().width, y)

def drawKinectRGB(screen, startPos=(0,0)):
    pass

def drawGyroInfo(screen, val, startPos = (0,0)):
    x,y = startPos
    maxX = x

    l = makeLabel('Gyro') 
    screen.blit(l, (x,y))
    y = y + l.get_rect().height
    maxX = max([maxX, x + l.get_rect().width])
    
    l = makeLabel("X: {:.3f}".format(val[0]))
    screen.blit(l,(x,y))
    y = y + l.get_rect().height
    maxX = max([maxX, x + l.get_rect().width])

    l = makeLabel("Y: {:.3f}".format(val[1]))
    screen.blit(l,(x,y))
    y = y + l.get_rect().height
    maxX = max([maxX, x + l.get_rect().width])

    l = makeLabel("Z: {:.3f}".format(val[2]))
    screen.blit(l,(x,y))
    y = y + l.get_rect().height
    maxX = max([maxX, x + l.get_rect().width])

    return (maxX,y)

def drawAccelInfo(screen, acc, startPos = (0,0)):
    x,y = startPos
    maxX = x

    l = makeLabel('Acceleration') 
    screen.blit(l, (x,y))
    y = y + l.get_rect().height
    maxX = max([maxX, x + l.get_rect().width])
    
    l = makeLabel("X: {:.3f}".format(acc[0]))
    screen.blit(l,(x,y))
    y = y + l.get_rect().height
    maxX = max([maxX, x + l.get_rect().width])

    l = makeLabel("Y: {:.3f}".format(acc[1]))
    screen.blit(l,(x,y))
    y = y + l.get_rect().height
    maxX = max([maxX, x + l.get_rect().width])

    l = makeLabel("Z: {:.3f}".format(acc[2]))
    screen.blit(l,(x,y))
    y = y + l.get_rect().height
    maxX = max([maxX, x + l.get_rect().width])

    return (maxX,y)

def drawUI(screen, le, cubeView):
    screen.fill((0,0,0))
    p = drawAccelInfo(screen, le.lastAcc, (20,20))
    p = drawGyroInfo(screen, le.lastGyro, (p[0]+20, 20))

    colX = p[0]+20
    rowY = p[1]+20

    p = drawBattery(screen, le.lastBattery, (colX, 20))
    p = drawPressure(screen, le.lastPressure, (colX, p[1]+20))

    l = makeLabel("PID: {:.3f}".format(le.v))
    screen.blit(l,(colX, p[1]+20))

    a = accToEuler(le.lastAcc)
    cubeView.angle = a
    s = cubeView.frame
    screen.blit(s, (20,rowY))

if __name__ == '__main__':
    # Initialize the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)
    pg.init()

    # create the orientation view
    cubeView = OrientationCubeView(100,100)
    cubeView.angle = EulerAngle(0,0,0)

    # Scan for Crazyflies and use the first one found
    print ("Scanning interfaces for Crazyflies...")
    available = cflib.crtp.scan_interfaces()
    print ("Crazyflies found:")
    for i in available:
        print (i[0])

    if len(available) > 0:
        screen = pg.display.set_mode((640, 480))
        chosen = "radio://0/80/2M" #available[0][0]

        pid1 = PIDController(7000, -200, -750)
        pid2 = PIDController(9000, 0, 100)

        pid1.target = 0.98
        pid2.target = 1.0

        currentPID = None
        
        le = CrazyflieController(chosen)
        le.v = 0
        done = False
        popThrottle = 20000
        gogo = False
        try:
            while not done:
                pg.time.delay(50)
                for event in pg.event.get():
                    if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                        done = True
                    elif event.type == pg.KEYDOWN:
                        if event.key == ord('x'):
                            gogo = False
                            le.throttleDown(90000)
                        elif event.key == ord('1'):
                            currentPID = pid1
                            gogo = True
                        elif event.key == ord('2'):
                            currentPID = pid2
                            gogo = True
                if done: break
                if gogo:
                    z = le.lastAcc[2]
                    v = currentPID.update(z)
                    le.v = v
                    le.throttleUp(v)
                drawUI(screen, le, cubeView)
                pg.display.flip()
        finally:
            le.stop()

    else:
        print ("No Crazyflies found, cannot run example")
