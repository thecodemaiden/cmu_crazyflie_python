# -*- coding: utf-8 -*-
#
#     ||          ____  _ __
#  +------+      / __ )(_) /_______________ _____  ___
#  | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
#  +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
#   ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
#
#  Copyright (C) 2014 Bitcraze AB
#
#  Crazyflie Nano Quadcopter Client
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA  02110-1301, USA.


import time, sys
from threading import Thread

import cflib
from cflib.crazyflie import Crazyflie

import logging
from cfclient.utils.logconfigreader import LogConfig

logging.basicConfig(level=logging.ERROR)

class CrazyflieController(object):
    """Example that connects to a Crazyflie and ramps the motors up/down and
    the disconnects"""
    def __init__(self, link_uri):
        """ Initialize and run the example with the specified link_uri """
        super(CrazyflieController, self).__init__()
        self._stop = False
        self._cf = Crazyflie()

        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)

        self._cf.open_link(link_uri)

        self.lastAcc = (0,0,0)
        self.lastGyro = (0,0,0)
        self.lastBattery = 0.0
        self.lastPressure = 0

        self._nextThrottle = 0
        self._nextYawRate = 0
        self._nextRoll =  0
        self._nextPitch = 0

        self._logfile = None

        self._sawBump = False

        print ("Connecting to %s" % link_uri)

    def checkBump(self):
        checked = self._sawBump
        self._sawBump = False
        return checked

    def _connected(self, link_uri):
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded."""

        # Start a separate thread to do the motor test.
        # Do not hijack the calling thread!
        Thread(target=self._run_motors).start()

        # The definition of the logconfig can be made before connecting
        self._lg_stab = LogConfig(name="Motion", period_in_ms=50)
        self._lg_stab.add_variable("pm.vbat", "float")
        self._lg_stab.add_variable("acc.x", "float")
        self._lg_stab.add_variable("acc.y", "float")
        self._lg_stab.add_variable("acc.z", "float")
        #self._lg_stab.add_variable("gyro.x", "float")
        #self._lg_stab.add_variable("gyro.y", "float")
        #self._lg_stab.add_variable("gyro.z", "float")

        # Adding the configuration cannot be done until a Crazyflie is
        # connected, since we need to check that the variables we
        # would like to log are in the TOC.
        try:
            self._cf.log.add_config(self._lg_stab)
            # This callback will receive the data
            self._lg_stab.data_received_cb.add_callback(self._stab_log_data)
            # This callback will be called on errors
            self._lg_stab.error_cb.add_callback(self._stab_log_error)
            # Start the logging
            self._lg_stab.start()
        except KeyError as e:
            print ("Could not start log configuration," \
                  "{} not found in TOC".format(str(e)))
        except AttributeError:
            print ("Could not add log config, bad configuration.")

    def _stab_log_error(self, logconf, msg):
        """Callback from the log API when an error occurs"""
        print ("Error when logging %s: %s" % (logconf.name, msg))

    def startLog(self, header=True, infix=None):
        if infix is not None:
            templ = 'copter_{}'.format(infix)
        else:
            templ = 'copter'
        filename = 'logs/{}_{}.log'.format(templ, int(time.time()))
        if header:
            with open(filename, 'a') as logFile:
                logFile.write('Time\tAcc\tPress\tBatt\n')
        self._logfile = filename

    def insertLogLine(self, line):
        if self._logfile is not None:
            with open(self._logfile, 'a') as logFile:
                logFile.write(line)

    def endLog(self):
        self._logfile = None

    def _stab_log_data(self, timestamp, data, logconf):
        """Callback froma the log API when data arrives"""

        if self._logfile is not None:
            with open(self._logfile, 'a') as logFile:
                outStr = '{}\t{}\t{}\t{}\n'.format(timestamp, data['acc.z'], data['pm.vbat'])
                logFile.write(outStr)

        self.lastAcc = (data['acc.x'], data['acc.y'], data['acc.z'])

        #zAcc = data['acc.z'] - 1
        zAcc = 1 - data['acc.z']
        if abs(zAcc >= 1.0):
            print ('Bump: {:.3f}'.format(zAcc))
            self._sawBump = True

        #self.lastGyro = (data['gyro.x'], data['gyro.y'], data['gyro.z'])
        self.lastPressure = 0#data['baro.pressure']
        self.lastBattery = data['pm.vbat']

    def _connection_failed(self, link_uri, msg):
        """Callback when connection initial connection fails (i.e no Crazyflie
        at the speficied address)"""
        print ("Connection to %s failed: %s" % (link_uri, msg))

    def _connection_lost(self, link_uri, msg):
        """Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range)"""
        print ("Connection to %s lost: %s" % (link_uri, msg))

    def _disconnected(self, link_uri):
        """Callback when the Crazyflie is disconnected (called in all cases)"""
        print ("Disconnected from %s" % link_uri)

    def resetAll(self):
        self._nextThrottle =0
        self._nextRoll = 0
        self._nextPitch = 0
        self._nextYawRate = 0

    def throttleUp(self, amnt=1500):
        self._nextThrottle += amnt
        if self._nextThrottle > 60000:
            self._nextThrottle = 60000
        if self._nextThrottle < 19000:
            self._nextThrottle = 19000

    def throttleDown(self, amnt=1500):
        self._nextThrottle -= amnt
        if self._nextThrottle < 20000:
            self._nextThrottle = 0;

    def rollLeft(self, amnt=1):
        self._nextRoll += amnt
        if self._nextRoll > 180:
            self._nextRoll = 180

    def rollRight(self, amnt=1):
        self._nextRoll -= amnt
        if self._nextRoll < -180:
            self._nextRoll = -180

    def pitchForward(self, amnt=1):
        self._nextPitch -= amnt
        if self._nextPitch < -180:
            self._nextPitch = -180

    def pitchBack(self, amnt=1):
        self._nextPitch += amnt
        if self._nextPitch > 180:
            self._nextPitch = 180

    def spinClockwise(self, amnt=1):
        self._nextYawRate -= amnt
        if self._nextYawRate < -360:
            self._nextYawRate = -360

    def spinCounterclockwise(self,amnt=1):
        self._nextYawRate += amnt
        if self._nextYawRate > 360:
            self._nextYawRate = 360

    def stop(self):
        self._stop = True

    def _run_motors(self):

        #Unlock startup thrust protection
        self._cf.commander.send_setpoint(0, 0, 0, 0)

        time.sleep(0.5)
        while not self._stop:
            pitch = self._nextPitch
            roll = self._nextRoll
            yawrate = self._nextYawRate
            throttle = int(self._nextThrottle)

            self._cf.commander.send_setpoint(roll, pitch, yawrate, throttle)
            time.sleep(0.1)
            self._nextYawRate = 0
        print ("Closing link (please wait)...")
        self._cf.commander.send_setpoint(0, 0, 0, 0)
        self._cf.close_link()
