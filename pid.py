import time

class PIDController(object):
    def __init__(self, kp=1, ki=0, kd=0):
        super(PIDController, self).__init__()
        self.current = 0
        self.target = 0
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.output = 0

        self.error_sum = 0
        self.last_error = 0
        self.last_time = -1

    def update(self, currValue):
        err = self.target - currValue
        self.current = currValue
        now = time.time()

        term_i = term_d = 0
        term_p = self.kp*err
        if self.last_time != -1:
            dt = now-self.last_time
            self.error_sum += err*dt
            term_i = self.ki*self.error_sum
            term_d = self.kd*(err - self.last_error)/dt
        self.last_time = now
        self.last_error = err

        self.output = term_p + term_i + term_d
        return self.output

